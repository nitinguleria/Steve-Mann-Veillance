package org.opencv.samples.colorblobdetect;

import org.abaq.wirelessveillometer.R;
import org.abaq.wirelessveillometer.R.id;
import org.abaq.wirelessveillometer.R.layout;
import org.abaq.wirelessveillometer.R.menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.os.Build;

public class SettingsActivity extends Activity {

	// UI elements
	EditText scaleBox;
	EditText offsetBox;
	
	// Variables
	double a = 1, b = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		// Get UI elements
		scaleBox = (EditText) findViewById(R.id.scaleEditText);
		offsetBox = (EditText) findViewById(R.id.offsetEditText);
		
		// Get values from main menu activity
		a = MainMenuActivity.a;
		b = MainMenuActivity.b;
		
		// Set current values in text boxes
		scaleBox.setText("" + a);
		offsetBox.setText("" + b);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// Save settings and return to the main menu
	public void saveAndExit(View v) {
		String aString, bString;
		
		// Get text box entries
		aString = scaleBox.getText().toString();
		bString = offsetBox.getText().toString();
		
		// Check that reasonable values were entered
		if(aString.matches("")) {
			aString = "1";
		}
		if(bString.matches("")) {
			bString = "0";
		}
		
		// Convert strings to doubles
		a = Double.parseDouble(aString);
		b = Double.parseDouble(bString);
		
		// Save values and exit
		MainMenuActivity.a = a;
		MainMenuActivity.b = b;
		finish();
	}

}
