package org.opencv.samples.colorblobdetect;

import java.lang.reflect.Method;

import org.abaq.wirelessveillometer.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class ConnectToWiFiActivity extends Activity {
	
	// UI elements
	RadioButton useHotSpotButton;
	RadioButton useWiFiButton;
	EditText networkNameBox;
	EditText networkPasswordBox;
	EditText arduinoIpBox;

	// WiFi AP state constants
	private static int WIFI_AP_STATE_UNKNOWN = -1;
    private static int WIFI_AP_STATE_DISABLING = 0;
    private static int WIFI_AP_STATE_DISABLED = 1;
    public static int WIFI_AP_STATE_ENABLING = 2;
    public static int WIFI_AP_STATE_ENABLED = 3;
    private static int WIFI_AP_STATE_FAILED = 4;
    
    // WiFi configuration
	private WifiManager wifi;
	WifiConfiguration config = new WifiConfiguration();
	
	// Operational information
	private int modeOfOperation;
	public static String arduinoIp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connect_to_wi_fi);
		
		// Get WiFi information and mode
		wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		modeOfOperation = MainMenuActivity.modeOfOperation;
		
		// Get radio buttons and information
		useHotSpotButton = (RadioButton) findViewById(R.id.useHotSpotButton);
		useWiFiButton = (RadioButton) findViewById(R.id.useWiFiButton);
		
		// Get text fields
		networkNameBox = (EditText) findViewById(R.id.networkNameEditText);
		networkPasswordBox = (EditText) findViewById(R.id.networkPasswordEditText);
		arduinoIpBox = (EditText) findViewById(R.id.arduinoIpEditText);
		
		// Set up default network connection mode and information based on operation mode
		if(modeOfOperation == 0) {
			useHotSpotButton.setChecked(true);
			useWiFiButton.setEnabled(false);
			networkNameBox.setText("AndroidAP");
			networkPasswordBox.setText("abaqabaqabaq");
			arduinoIpBox.setText("192.168.43.70");
		} else if(modeOfOperation == 1) {
			useHotSpotButton.setChecked(true);
			networkNameBox.setText("AndroidAP");
			networkPasswordBox.setText("abaqabaqabaq");
			arduinoIpBox.setText("192.168.43.70");
		} else if(modeOfOperation == 2) {
			useWiFiButton.setChecked(true);
			useHotSpotButton.setEnabled(false);
			arduinoIpBox.setEnabled(false);
			networkNameBox.setText("AndroidAP");
			networkPasswordBox.setText("abaqabaqabaq");
		}
	}
	
	// Clear default password if the user has entered a custom name
	public void networkNameBoxPressed(View v) {
		networkPasswordBox.setText("");
	}
	
	// Doesn't do anything with network settings, and goes to main activity
	public void goToBeginNoNetwork(View v) {
		// Still read the Arduino IP information
		arduinoIp = arduinoIpBox.getText().toString();
		if(arduinoIp.matches("")) {
			Toast.makeText(this, "Please enter an IP address for the Arduino", Toast.LENGTH_LONG).show();
		    return;
		}
		
		// Continue to main activity
		Intent switchActivity = new Intent(this, ColorBlobDetectionActivity.class);
		startActivity(switchActivity);
	}
	
	// Sets up the selected network mode and starts the main activity
	public void goToBegin(View v) {
		// Get network information
		final String networkName = networkNameBox.getText().toString();
		final String networkPassword = networkPasswordBox.getText().toString();
		arduinoIp = arduinoIpBox.getText().toString();
		
		// Check if information was entered
		if(networkName.matches("")) {
			Toast.makeText(this, "Please enter a network name (SSID)", Toast.LENGTH_LONG).show();
		    return;
		}
		
		if(networkPassword.matches("")) {
			Toast.makeText(this, "Please enter a network password", Toast.LENGTH_LONG).show();
		    return;
		}
		
		if(arduinoIp.matches("")) {
			Toast.makeText(this, "Please enter an IP address for the Arduino", Toast.LENGTH_LONG).show();
		    return;
		}
		
		// Attempt to create or hotspot if that is the selected mode
		if(useHotSpotButton.isChecked()) {
			int state = WIFI_AP_STATE_UNKNOWN;
			int adjustment = 0;
			
			// Get current WiFi state
			try {
				Method wifiMethod = wifi.getClass().getMethod("getWifiApState");
				state = (Integer) wifiMethod.invoke(wifi);
			} catch (Exception e) {
				Log.d("error", "Error getting WiFi AP state");
				e.printStackTrace();
			}

			// Adjust newer information to match old standards
			if(state >= 10) {
				adjustment = 10;
			}
			
			WIFI_AP_STATE_DISABLING = 0 + adjustment;
	        WIFI_AP_STATE_DISABLED = 1 + adjustment;
	        WIFI_AP_STATE_ENABLING = 2 + adjustment;
	        WIFI_AP_STATE_ENABLED = 3 + adjustment;
	        WIFI_AP_STATE_FAILED = 4 + adjustment;
	        
	        // Configure network information
	        config.SSID = networkName; // Add quotes around this if it's not working
	        config.preSharedKey = networkPassword; // Add quotes around this if it's not working
	        config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
	        config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
	        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
	        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
	        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
	        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	        
	        // Enable the hot spot
	        new SetWifiAPTask(true, false, this).execute();
		}
		
		// Attempt to connect to WiFi network if that is the selected mode
		if(useWiFiButton.isChecked()) {
			// Turn off WiFi hot spot if it is active
	        new SetWifiAPTask(false, false, this).execute();
	        
	        try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				Log.d("error", "Error while sleeping thread");
				e.printStackTrace();
			}
	        
			// Configure network information
	        config.SSID = "\"" + networkName + "\"";
	        config.preSharedKey = "\"" + networkPassword + "\"";
	        config.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
	        config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
	        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
	        config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
	        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
	        config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	        //config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	        
	        // Add and connect to network
	        Log.d("information", "Attempting to connect to " + networkName + ", password " + networkPassword);
	        
	        int netId = wifi.addNetwork(config);
	        if(netId == -1) {
		        Log.d("error", "Error: could not create new network.");
	        }
	        
	        boolean results = wifi.enableNetwork(netId, true);
	        if(results == false) {
		        Log.d("error", "Error: unable to connect to network.");
	        }
	        
	        Log.d("information", "Waiting for connection...");
	        while(wifi.getWifiState() != WifiManager.WIFI_STATE_ENABLED);
	        Log.d("information", "Connected");
		}
		
		// Continue to main activity
		Intent switchActivity = new Intent(this, ColorBlobDetectionActivity.class);
		startActivity(switchActivity);
	}
	
	// Enables or disables WiFi AP hot spot
	class SetWifiAPTask extends AsyncTask<Void, Void, Void> {
        boolean mMode; //enable or disable wifi AP
        boolean mFinish; //finalize or not (e.g. on exit)
        ProgressDialog d;

        /**
         * enable/disable the wifi ap
         * @param mode enable or disable wifi AP
         * @param finish finalize or not (e.g. on exit)
         * @param context the context of the calling activity
         * @author http://stackoverflow.com/a/7049074/1233435
         */
        public SetWifiAPTask(boolean mode, boolean finish, Context context) {
            mMode = mode;
            mFinish = finish;
            d = new ProgressDialog(context);
        }

        /**
         * do before background task runs
         * @author http://stackoverflow.com/a/7049074/1233435
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            d.setTitle("Turning WiFi hot spot " + (mMode ? "on" : "off") + "...");
            d.setMessage("...please wait a moment.");
            d.show();
        }

        /**
         * do after background task runs
         * @param aVoid
         * @author http://stackoverflow.com/a/7049074/1233435
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                d.dismiss();
                //MainActivity.updateStatusDisplay();
            } catch (IllegalArgumentException e) {
            	Log.d("error", "Error dismissing dialog");
            	e.printStackTrace();
            };
            if (mFinish){
                finish();
            }
        }

        /**
         * the background task to run
         * @param params
         * @author http://stackoverflow.com/a/7049074/1233435
         */
        @Override
        protected Void doInBackground(Void... params) {
        	try {
        		if(mMode) {
        			wifi.setWifiEnabled(false);
    	            Method method1 = wifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
    	            method1.invoke(wifi, config, true); // true
    	            Method method2 = wifi.getClass().getMethod("getWifiApState");
    	            method2.invoke(wifi);
        		} else {
    	            Method method1 = wifi.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
    	            method1.invoke(wifi, config, false); // true
    	            Method method2 = wifi.getClass().getMethod("getWifiApState");
    	            method2.invoke(wifi);
        			wifi.setWifiEnabled(true);
        		}
	        } catch (Exception e) {
	        	Log.d("error", "Error while enabling or disabling WiFi AP");
	        	e.printStackTrace();
	        }
            return null;
        }
    }
    
	// Network help popup
    public void helpPopup(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This screen lets you set up the network connection to the Arduino. Currently, using a WiFi hot spot is the recommended method to do this.\n\n"
        		
        		         + "The network name and password should be left as-is (they are hard-coded on the Arduino as SSID: AndroidAP and Password: abaqabaqabaq).\n\n"
        		         
        		         + "The Arduino's IP changes from device to device. For Steve's Galaxy S5, use the IP provided (192.168.43.70). For other devices, "
        		         + "connect the Arduino to a computer via USB, and read the IP using the Serial Monitor utility (included in the Arduino IDE).\n\n"
        		         
        		         + "If you are having any issues with the Arduino, further help is available in the main menu, under \"Help with Arduino\".")
               .setPositiveButton("OK", null);
        
        builder.create().show();
    }
}
